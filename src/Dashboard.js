import React from 'react';
import {
  Card,
  Button,
  CardTitle,
  CardText,
  Jumbotron,
  CardHeader,
  CardBody,
   Row,
   Col } from 'reactstrap';


export const DashBoard = () => {

   return (
      <div>
      <div className = "main_container">
          <div className = "img1">
              <div className = "dashboard_content">
                <h1 style = {{fontSize: '3.5em', marginTop: "5px"}}>Welcome</h1>
                <br /><br />
                  <div className = "main_dashboard_content">
                    <p> Login to take your attendance, it is important to note that this Portal is for student only.
                    </p>
                    <p>If you are a Lecturer or Staff, please click <a href="/">here..</a></p>
                    <p>Its simple and easy, just <a href="/Loggedin"> sign-in</a> here to continue</p>
                  </div>
              </div>
          </div>
      </div>
    </div>
  );
}



export const MyJumbotron = (props) => {
  return (
    <div className="positioner" style={{color:"white"}}>
      <Jumbotron style= {{  background: 'linear-gradient(to right, rgba(255,0,0,0), brown)', paddingTop: '0px'}}>
        <h1 className="display-4">News Board</h1>
        <h4>Extension of Deadline for Payment of Fees/Registration</h4>
        <p className="lead" >
        Following passionate appeals by the Student Union Government, the Vice-Chancellor has magnanimously approved on behalf of the Senate,
        an extension of the deadline for payment of fees and registration for 2017/2018 Academic Session up to February 15th, 2018.
        Students who are yet to pay fees and register are advised to avail themselves...
        </p>
        <hr className="my-2" />
        <p>Validation of Entry Requirements for Redistributed SMAT Students</p>
        <p className="lead">
          <Button color="primary">Learn More</Button>
        </p>
      </Jumbotron>
    </div>
  );
};



export const FooterCards = (props) => {
  return (
  <div className="Counter-Fluid">
    <Row>
    <Col >
      <div>
        <Card className = "cards">
          <CardHeader tag="h3">Scholarships</CardHeader>
          <CardBody>
            <CardTitle>Available Scholarships</CardTitle>
            <CardText>
              <ul>
                <li>Fully Funded Scholarships in Italy for Students of Developing Countries, 2018</li>
                <li>SINGA Fully Funded Awards for Students in Singapore, 2019</li>
                <li>Japanese Government MEXT Scholarships for Foreign  Students</li>
                <li>Australian Government Awards Fully Funded BSc and Masters Scholarships, 2018</li>

             </ul>
            </CardText>
            <Button >Read more...</Button>
          </CardBody>
        </Card>
      </div>
    </Col>
    <Col >
      <div>
        <Card className = "cards">
          <CardHeader tag="h3">ICT in FUTO</CardHeader>
          <CardBody>
            <CardTitle>Historical Background of ICT in FUTO</CardTitle>
            <CardText>Information and Communications Technology (ICT) refers to all equipment
            and applications that involve communications such as computers, cell phones, televisions, radio
            and satellite systems as well as the various services and applications associated with them such as video
            conferencing and distant learning. FUTO Information and.....</CardText>
            <Button> Read more...</Button>
          </CardBody>
        </Card>
      </div>
      </Col>
      <Col>
        <div>
          <Card className = "cards">
            <CardHeader tag="h3">Examination</CardHeader>
            <CardBody>
              <CardTitle>Exam Time Table</CardTitle>
              <CardText>From the desk of the Head of the department Prof B.C Asiegwu.
              The updated exam time table for this semester is out. Every student is reqiured to login to his or her Portal
              to access this time table. Examination this semester is taken based on students percentage of attendance in class.
              Every student is required to generate a copy of the attendance sheet while coming for the exam</CardText>
              <Button >Read more...</Button>
            </CardBody>
          </Card>
        </div>
      </Col>

    </Row>
  </div>
  );
};

export const FooterItself = (props) => {
  return (
    <div>
      <div className = "footerItself" >
          <hr style = {{maxWidth: "90%"}}/>
          <a href = "/"> Home | </a>
           <a href = "/"> Logout | </a>
           <a href = "/">Register | </a>
           <a href = "/Loggedin"> Login </a>
          <p>Developed and designed by Eric</p>
      </div>
    </div>
  );
};
