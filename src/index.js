import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import NaviApp from './NavApp';
import {CalenderSheet} from './AttendanceCalender';
import {DashBoard, FooterItself} from './Dashboard';
import { BrowserRouter as Router, Route } from 'react-router-dom';

class AppAll extends React.Component {
  render(){
    return (
      <div>
  <Router>
     <div>
       <Route path="/" component={NaviApp} />
       <Route exact path="/" component={DashBoard} />
       <Route path="/Loggedin" component={CalenderSheet} />
   </div>
 </Router>
 <FooterItself />
</div>

    );
  }
}


ReactDOM.render(<AppAll />, document.getElementById('root'));
