
# Project Anatomy.
This react app is generated using the *create-react-app* script and is built solely with Javascript.

# Installing.
To install and run app, use the command below.
```sh
npm i && npm run start
```
